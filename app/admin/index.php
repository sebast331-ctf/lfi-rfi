<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Bootstrap 101 Template</title>

    <!-- Bootstrap -->
    <link href="/css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <style type="text/css">
        body {
            padding-top: 50px;
          }
          .starter-template {
            padding: 40px 15px;
            text-align: center;
          }
    </style>
  </head>

  <body>
    <?php include_once('../includes/navbar.php'); ?>
  
    <div class="container">

      <div class="starter-template">
          <?php if (array_key_exists('secret_key', $_GET) && $_GET['secret_key'] === 'fUPNreZfTjoVx8aSJiCPXeRs') { ?>
            <div class="alert alert-success">
                <h2>Bienvenue admin!<br><br>FLAG-9ka4UmR2pVLBrFY5d3hDtWU2</h2>
            </div>
          <?php } else { ?>
            <div class="alert alert-danger">
                <h2>Accès refusé!</h2>
            </div>
          <?php } ?>
      </div>

    </div><!-- /.container -->

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="/js/bootstrap.min.js"></script>
  </body>
</html>