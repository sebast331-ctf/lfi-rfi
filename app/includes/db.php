<?php
define('DB_HOST', '192.168.25.32');
define('DB_USER', 'db_user_app');
define('DB_PASS', 'FLAG{FICHIERS_DE_BACKUPS}');
define('DB_NAME', 'app_magasin');

function connect() {
    $conn = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

    if ($mysqli->connect_errorno) {
        die('Erreur de connexion');
    }

    return $conn;
}