<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="/">VerySekur Inc.</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
        <ul class="nav navbar-nav">
            <li class="<?php echo ($page == 'includes/page_home.php' ? 'active' : '');?>"><a href="/?page=includes/page_home.php">Accueil</a></li>
            <li class="<?php echo ($page == 'includes/page_about.php' ? 'active' : '');?>"><a href="/?page=includes/page_about.php">À propos</a></li>
            <li class="<?php echo ($page == 'includes/page_contact.php' ? 'active' : '');?>"><a href="/?page=includes/page_contact.php">Contact</a></li>
            <li class=""><a href="/admin/index.php">Administration</a></li>
            <li class=""><a href="http://192.168.99.12">Intranet</a></li>
        </ul>
        </div><!--/.nav-collapse -->
    </div>
</nav>