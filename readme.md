# Instructions
Pour lancer l'application :
```
git clone https://gitlab.com/sebast331-ctf/lfi-rfi
cd lfi-rfi
docker-compose up
```
L'application sera disponible à l'adresse http://localhost:8000

# Flags
Les flags sont "hardcoded" dans les fichiers suivants :
- ./app/includes/db.php.bak -- Misconfiguration (ligne 3)
- ./app/.htaccess -- LFI de base (ligne 1)
- ./app/admin/index.php -- LFI + PHP filter (ligne 39)
- ./app_intranet/secrets.txt -- RFI

# Configuration
Cette application est configurée pour permettre l'inclusion locale et l'inclusion à distance de fichiers. Les options `allow_url_fopen` et `allow_url_include` sont activées dans `php.ini`.

# Défis
## LFI
1. Lire le fichier `/etc/passwd`
1. Lire le fichier `.htaccess`
1. Un fichier contient un compte pour se connecter à la base de données (fictive). Trouvez ce fichier et lisez-le.
1. Obtenez accès à la section Admin
1. Obtenir un *reverse shell* avec du Log Poisoning

## RFI
1. Obtenez accès à l'Intranet et obtenez le flag
1. Exécutez des commandes sur le serveur
1. Obtenez un *reverse shell* sur le serveur
