<?php
$server_name = $_SERVER['SERVER_NAME'];
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Intranet</title>
</head>
<body>
    <h1>Bienvenue sur l'Intranet</h1>
    <p>Les mots de passes sont ici : <a href="http://<?php echo $server_name; ?>/secrets.txt">suivez ce lien</a></p>
</body>
</html>